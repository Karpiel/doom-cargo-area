# Doom - Cargo Area

![](doc/pics/Cargo_Area_1_1b.png)

## General Description

Cargo Area in the vein of Doom 1.

 Version 1.1

This map is created specifically for GZDoom and Brutal Doom in mind. Jumping and freelook are very helpful but you can walkthrough this map without those features.

The main idea for this map was to create a tech base filled with boxes and crates. Narrow and crammed spaces intermixed with more open areas. There are places that should work as covers and safe spots allowing you to plan tactics.


## Map Features

- New levels              : 1
- Sounds                  : No
- Music                   : No
- Graphics                : No
- Dehacked/BEX Patch      : No
- Demos                   : No
- Other                   : No
- Other files required    : Nothing required


*Play Information*

- Game                    : DOOM 1
- Map #                   : E1M1
- Single Player           : Designed for
- Cooperative 2-4 Player  : No
- Deathmatch 2-4 Player   : No
- Other game styles       : None
- Difficulty Settings     : Yes


*Construction*

- Base                    : New from scratch
- Build Time              : Around 2 or 3 weeks 
- Editor(s) used          : GZDoom Builder and Ultimate Doom Builder
- Known Bugs              : Unknown
- May Not Run With        : Unknown
- Tested With             : GZDoom 3.0.1 and 4.2.2, Brutal Doom 21 RC1 FIX


## Links

[Doomworld.com forum thread](https://www.doomworld.com/forum/topic/115754-new-map-cargo-area-map-for-doom1-v10/)

[Author Fan Page](https://www.facebook.com/World-Reloaded-107897790984291)
